<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->exchange_declare('broadcast', 'fanout', false, true, false);
list($queue,,) = $ch->queue_declare('', false, true, false, true);
echo "queue: ", $queue, "\n";
$ch->queue_bind($queue, 'broadcast');


function process_message($msg) {
    echo getmypid(), ": ", $msg->body, "\n";
    
    $channel = $msg->delivery_info['channel'];

    $msg_tag = $msg->delivery_info['delivery_tag'];

    $channel->basic_ack($msg_tag);
}

/*
    queue: Queue from where to get the messages
    consumer_tag: Consumer identifier
    no_local: Don't receive messages published by this consumer.
    no_ack: Tells the server if the consumer will acknowledge the messages.
    exclusive: Request exclusive consumer access, meaning only this consumer can access the queue
    nowait:
    callback: A PHP Callback
*/
$ch->basic_consume($queue, '', false, false, false, false, 
                    'process_message');

while (count($ch->callbacks)) {
    $ch->wait();
}