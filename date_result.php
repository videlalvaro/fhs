<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->exchange_declare('result', 'direct', false, true, false);
list($queue,,) = $ch->queue_declare();
$ch->queue_bind($queue, 'result');

function process_message($msg) {
    $result = json_decode($msg->body, true);
    echo $result['date'], "\n";
}

$ch->basic_consume($queue, '', false, false, false, false, 
                    'process_message');

while (count($ch->callbacks)) {
    $ch->wait();
}