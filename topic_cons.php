<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

list($queue,,) = $ch->queue_declare('', false, true, false, true);

echo "queue: ", $queue, "\n";

$bk = $argv[1];
echo "bk: ", $bk, "\n";

$ch->queue_bind($queue, 'news', $bk);

function process_message($msg) {
    echo getmypid(), ": ", $msg->body, "\n";
}

$ch->basic_consume($queue, '', false, true, false, false, 
                    'process_message');

while (count($ch->callbacks)) {
    $ch->wait();
}