<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$rk = $argv[2];

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$msg_body = $argv[1];
$msg = new AMQPMessage($msg_body);

// name, type, passive, durable, auto_delete
$ch->exchange_declare('news', 'topic', false, true, false);

echo "rk: ", $rk, "\n";
$ch->basic_publish($msg, 'news', $rk);

$ch->close();
$conn->close();