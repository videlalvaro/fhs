<?php

include(__DIR__ . '/config.php');
include(__DIR__ . '/pipes_und_filters.php');

use PhpAmqpLib\Connection\AMQPConnection;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

class DateFilter implements IFilter {

    public function process($body) {
        $input = json_decode($body, true);

        $result = array(
            'date' => date($input['format'], $input['timestamp'])
        );

        return json_encode($result);
    }
}

$filter = new DateFilter();
$pipe = new Pipe($ch, 'date', 'result', $filter);
$pipe->start();
