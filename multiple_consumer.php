<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$queues = array('a', 'b', 'c', 'd', 'e', 'f');

foreach ($queues as $q) {
    $ch->queue_declare($q, false, true, false, false);
}

function process_message($msg) {
    $routing_key = $msg->delivery_info['routing_key'];

    if ($msg->body == 'quit') {
        $ch = $msg->delivery_info['channel'];
        $consumer_tag = $msg->delivery_info['consumer_tag'];
        echo "stoping: ", $routing_key, " consumer\n";
        $ch->basic_cancel($consumer_tag);
    } else {
        echo "queue: ", $routing_key, " got a message\n";
    }
}

foreach ($queues as $q) {
    $ch->basic_consume($q, '', false, true, false, false, 
                       'process_message');
}

while (count($ch->callbacks)) {
    $ch->wait();
}