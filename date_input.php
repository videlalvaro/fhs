<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$timestamp = sprintf("%s", time());
echo $timestamp, "\n";
$msg_body = json_encode(array('timestamp' => $timestamp));
$msg = new AMQPMessage($msg_body, array('delivery_mode' => 2));

$ch->basic_publish($msg, 'date_format');

$ch->close();
$conn->close();