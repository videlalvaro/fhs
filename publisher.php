<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

define('TRANSIENT',  1);
define('PERSISTENT', 2);

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$msg_body = implode(' ', array_slice($argv, 1));
$msg = new AMQPMessage($msg_body, 
                        array('delivery_mode' => PERSISTENT));

$ch->basic_publish($msg, '', 'hallo');

$ch->close();
$conn->close();