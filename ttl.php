<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$exchange = 'events';
$queue = 'events_queue';

$dlx = 'events_dlx';
$dlx_q = 'events_queue_dlx';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

// DLX part:
$ch->exchange_declare($dlx, 'direct', false, true, false);
$ch->queue_declare($dlx_q, false, true, false, false, false);
$ch->queue_bind($dlx_q, $dlx);

// Our process queue setup:
$args = array(
    "x-max-length" => array("I", 10),
    "x-dead-letter-exchange" => array("S", $dlx),
);

$ch->queue_declare($queue, false, true, false, false, false, $args);
$ch->exchange_declare($exchange, 'direct', false, true, false);
$ch->queue_bind($queue, $exchange);

for ($i = 1; $i <= 100; $i++) {
    $msg_body = sprintf('%s', $i);
    $msg = new AMQPMessage($msg_body);
    $ch->basic_publish($msg, $exchange);
}
 
$ch->close();
$conn->close();