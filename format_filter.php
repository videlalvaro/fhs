<?php

include(__DIR__ . '/config.php');
include(__DIR__ . '/pipes_und_filters.php');

use PhpAmqpLib\Connection\AMQPConnection;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

class FormatFilter implements IFilter {

    public function process($body) {
        $input = json_decode($body, true);

        $result = array(
            'timestamp' => $input['timestamp'],
            'format' => "D M j G:i:s T Y"
        );

        return json_encode($result);
    }
}

$filter = new FormatFilter();
$pipe = new Pipe($ch, 'date_format', 'date', $filter);
$pipe->start();