<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->exchange_declare('char_count_client', 'direct', false, true, false);
$ch->queue_declare('char_count_client_q', false, true, false, false);
$ch->queue_bind('char_count_client_q', 'char_count_client');

function process_message($msg) {
    echo $msg->body, "\n";

    $ch = $msg->delivery_info['channel'];
    $consumer_tag = $msg->delivery_info['consumer_tag'];
    $ch->basic_cancel($consumer_tag);
}

// no_ack = true
$ch->basic_consume('char_count_client_q', '', false, true, false, false, 
                   'process_message');

$msg_body = $argv[1];
$msg = new AMQPMessage($msg_body);
$ch->basic_publish($msg, 'char_count');

while (count($ch->callbacks)) {
    $ch->wait();
}

$ch->close();
$conn->close();