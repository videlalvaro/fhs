<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$msg_body = $argv[1];
$msg = new AMQPMessage($msg_body);
$ch->basic_publish($msg, '', $argv[2]);

$ch->close();
$conn->close();
