<?php

require_once __DIR__ . '/../vendor/autoload.php';
use PhpAmqpLib\Message\AMQPMessage;

// input message: array('timestamp' => time());
// first filter: array('format' => "D M j G:i:s T Y", 'timestamp' => '12345678');
// result filter: array('date' => "Fri May 16 11:57:17 CEST 2014");

interface IFilter
{
    public function process($msg);
}

class Pipe
{
    protected $ch;
    protected $from;
    protected $to;
    protected $filter;

    public function __construct($ch, $from, $to, IFilter $filter)
    {
        $this->ch = $ch;
        $this->from = $from;
        $this->to = $to;
        $this->filter = $filter;
    }

    public function start()
    {
        // passive, durable, exclusive, auto_delete, 
        $this->ch->queue_declare($this->from, false, true, false, false);
        // passive, durable, auto_delete,
        $this->ch->exchange_declare($this->from, 'direct', false, true, false);
        $this->ch->queue_bind($this->from, $this->from);

        // prefetch_size, prefetch_count, a_global
        $this->ch->basic_qos(null, 10, null);

        $this->ch->basic_consume($this->from, '', 
                            false, false, false, false, 
                            array($this, 'handle_message'));

        while (count($this->ch->callbacks)) {
            $this->ch->wait();
        }
    }

    public function handle_message($msg)
    {
        $result = $this->filter->process($msg->body);

        $result_msg = new AMQPMessage($result, array('delivery_mode' => 2));
        $this->ch->basic_publish($result_msg, $this->to);

        $this->ack($msg);
    }
    
    protected function ack($msg)
    {
        $channel = $msg->delivery_info['channel'];
        $msg_tag = $msg->delivery_info['delivery_tag'];
        $channel->basic_ack($msg_tag);
    }
}