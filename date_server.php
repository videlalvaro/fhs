<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->exchange_declare('date_server', 'direct', false, true, false);
$ch->queue_declare('date_server_q', false, true, false, false);
$ch->queue_bind('date_server_q', 'date_server');

function process_message($msg) {
    $timestamp = $msg->body;
    
    $ch = $msg->delivery_info['channel'];
    $reply_to = $msg->get('reply_to');
    $correlation_id = $msg->get('correlation_id');
    
    $msg_body = date("D M j G:i:s T Y", $timestamp);
    $reply = new AMQPMessage($msg_body, 
                    array('correlation_id' => $correlation_id));
    
    echo "replying to: ", $reply_to, "\n";
    
    $ch->basic_publish($reply, '', $reply_to);
}

// no_ack = true
$ch->basic_consume('date_server_q', '', false, true, false, false, 
                   'process_message');

while (count($ch->callbacks)) {
    $ch->wait();
}