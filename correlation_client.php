<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class MultipleRpcClient {
    
    protected $ch;
    
    protected $req_id = 0;
    protected $request = array();
    protected $request_count = 0;
    
    public function __construct($ch) {
        $this->ch = $ch;
    }
    
    public function request($msg_body, $server) {
        list($queue,,) = $this->ch->queue_declare();
        
        $correlation_id = $this->next_id();
        
        $this->request[$correlation_id] = null;
        $this->request_count++;
        
        $msg = new AMQPMessage($msg_body,
                               array(
                                   'reply_to' => $queue,
                                   'correlation_id' => $correlation_id
                               ));

        $this->ch->basic_consume($queue, '', false, true, false, false, 
                          array($this, 'handle_message'));

        $this->ch->basic_publish($msg, $server);
    }
    
    public function get_replies() {
        while ($this->request_count != 0) {
            $this->ch->wait();
        }
        return array_values($this->request);
    }
    
    public function handle_message($msg) {
        $correlation_id = $msg->get('correlation_id');
        
        if (array_key_exists($correlation_id, $this->request) &&
                $this->request[$correlation_id] == null) {
            $this->request[$correlation_id] = $msg->body;
            $this->request_count--;
        }
    }
    
    protected function next_id() {
        $this->req_id++;
        return $this->req_id;
    }
}

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$mc = new MultipleRpcClient($ch);

$mc->request('some words', 'char_count');
$mc->request('something else', 'char_count');
$mc->request(time(), 'date_server');

$replies = $mc->get_replies();

foreach($replies as $r) {
    echo $r, "\n";
}

$ch->close();
$conn->close();