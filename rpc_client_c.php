<?php

include(__DIR__ . '/config.php');
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

//durable = false
//auto_delete = true
list($queue,,) = $ch->queue_declare();

function process_message($msg) {
    echo $msg->body, "\n";

    $ch = $msg->delivery_info['channel'];
    $consumer_tag = $msg->delivery_info['consumer_tag'];
    $ch->basic_cancel($consumer_tag);
}

// no_ack = true
$ch->basic_consume($queue, '', false, true, false, false, 
                   'process_message');

$msg_body = $argv[1];
$msg = new AMQPMessage($msg_body, 
                       array(
                           'reply_to' => $queue
                       ));

$ch->basic_publish($msg, 'char_count');

while (count($ch->callbacks)) {
    $ch->wait();
}

$ch->close();
$conn->close();